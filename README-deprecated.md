# Old Infracost Gitlab-CI template

## Deprecation notice

⚠️ This README is deprecated. ⚠️

👉  We recommend everyone to use [the new README.md](README.md) for our improved method for running Infracost with GitLab CI! Follow our [migration guide](https://www.infracost.io/docs/guides/gitlab_ci_migration/) for more details. 👈

---

This GitLab-CI template runs [Infracost](https://infracost.io) against merge requests and automatically adds a merge request comment showing the cost estimate difference for the planned state. See [this repo for a demo](https://gitlab.com/infracost/gitlab-ci-demo).

The template uses the latest version of Infracost by default as we regularly add support for more cloud resources. If you run into any issues, please join our [community Slack channel](https://www.infracost.io/community-chat); we'd be happy to guide you through it.

As mentioned in our [FAQ](https://infracost.io/docs/faq), no cloud credentials or secrets are sent to the Cloud Pricing API. Infracost does not make any changes to your Terraform state or cloud resources.

<img src="screenshot.png" width=557 alt="Example screenshot" />

## Table of Contents

* [Usage](#usage)
* [Environment variables](#environment-variables)
  * [Integration variables](#integration-variables)
  * [CLI variables](#cli-variables)
* [Contributing](#contributing)

# Usage

1. Add CI environment variables for `INFRACOST_API_KEY`, `GITLAB_TOKEN` and any other required credentials to your GitLab repo (e.g. `AWS_ACCESS_KEY_ID`). Mask variables are recommended.

2. Create/update the `.gitlab-ci.yml` file in your repo with the following content. Use the Variables and Environment Variables section above to decide which `variables` options work for your Terraform setup. The following example uses `path` to specify the location of the Terraform directory and `terraform_plan_flags` to specify the variables file to use when running `terraform plan`.

  ```
    stages:
      - infracost

    include:
      # Use a specific version of the template instead of master if locking the template is preferred
      remote: 'https://gitlab.com/infracost/infracost-gitlab-ci/-/raw/master/infracost.yml'

    infracost-job:
      only:
        - merge_requests
      stage: infracost
      extends: .infracost
      variables:
        path: "terraform"
        terraform_plan_flags: "-var-file=my.tfvars"
  ```

3. Send a new merge request to change something in Terraform that costs money; a comment should be posted on the merge request. Check the GitLab CI Pipeline logs and [this page](https://www.infracost.io/docs/troubleshooting/) if there are issues.

# Environment variables

There are two sets of environment variables: ones that are used by this integration, and ones that are used by the Infracost CLI. Both can be specified in the `variables:` block of your `.gitlab-ci.yml` file. GitLab Protected or Masked [custom variables](https://docs.gitlab.com/ee/ci/variables/README.html#custom-environment-variables) can be used for sensitive environment values.

## Integration variables

### `path`

**Optional** Path to the Terraform directory or JSON/plan file. Either `path` or `config_file` is required.

### `terraform_plan_flags`

**Optional** Flags to pass to the 'terraform plan' command, e.g. `"-var-file=my.tfvars -var-file=other.tfvars"`. Applicable when path is a Terraform directory.

### `terraform_workspace`

**Optional** The Terraform workspace to use. Applicable when path is a Terraform directory. Only set this for multi-workspace deployments, otherwise it might result in the Terraform error "workspaces not supported".

### `usage_file`

**Optional** Path to Infracost [usage file](https://www.infracost.io/docs/usage_based_resources#infracost-usage-file) that specifies values for usage-based resources, see [this example file](https://github.com/infracost/infracost/blob/master/infracost-usage-example.yml) for the available options.

### `config_file`

**Optional** If your repo has **multiple Terraform projects or workspaces**, define them in a [config file](https://www.infracost.io/docs/config_file/) and set this input to its path. Their results will be combined into the same diff output. Cannot be used with path, terraform_plan_flags or usage_file inputs. 

### `show_skipped`

**Optional** List unsupported and free resources, some of which might be free, at the bottom of the Infracost output (default is false).

### `post_condition`

**Optional** A JSON string describing the condition that triggers merge request comments, can be one of these:
- `'{"has_diff": true}'`: only post a comment if there is a diff. This is the default behavior.
- `'{"always": true}'`: always post a comment.
- `'{"percentage_threshold": 0}'`: absolute percentage threshold that triggers a comment. For example, set to 1 to post a comment if the cost estimate changes by more than plus or minus 1%.

### `sync_usage_file` (experimental)

**Optional**  If set to `true` this will create or update the usage file with missing resources, either using zero values or pulling data from AWS CloudWatch. For more information see the [Infracost docs here](https://www.infracost.io/docs/usage_based_resources#1-generate-usage-file). You must also specify the `usage_file` input if this is set to `true`.

### `GITLAB_TOKEN`

**Required** GitLab token used to post comments, for example a [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#project-access-tokens) or if that's not an option, then a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

### `GIT_SSH_KEY`

**Optional** If you're using Terraform modules from private Git repositories you can set this environment variable to your private Git SSH key so Terraform can access your module.

### `SLACK_WEBHOOK_URL`

**Optional** Set this to also post the merge request comment to a [Slack Webhook](https://slack.com/intl/en-tr/help/articles/115005265063-Incoming-webhooks-for-Slack), which should post it in the corresponding Slack channel.

## CLI variables

This section describes the main environment variables that can be used with the Infracost CLI. Other supported environment variables are described in the [this page](https://www.infracost.io/docs/integrations/environment_variables). GitLab Protected or Masked [custom variables](https://docs.gitlab.com/ee/ci/variables/README.html#custom-environment-variables) can be used for sensitive environment values.

Terragrunt users should also read [this page](https://www.infracost.io/docs/iac_tools/terragrunt). Terraform Cloud/Enterprise users should also read [this page](https://www.infracost.io/docs/iac_tools/terraform_cloud_enterprise).

### `INFRACOST_API_KEY`

**Required** To get an API key [download Infracost](https://www.infracost.io/docs/#quick-start) and run `infracost auth login`.

### Cloud credentials

**Required** You do not need to set cloud credentials if you use Terraform Cloud/Enterprise's remote execution mode, instead you should follow [this page](https://www.infracost.io/docs/iac_tools/terraform_cloud_enterprise).

For all other users, the following is needed so Terraform can run `init`:
- Azure users should read [this section](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/service_principal_client_secret) to see which environment variables work for their use-case.
- AWS users should set `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`, or read [this section](https://registry.terraform.io/providers/hashicorp/aws/latest/docs#environment-variables) of the Terraform docs for other options. If your Terraform project uses multiple AWS credentials you can configure them using the [Infracost config file](https://www.infracost.io/docs/multi_project/config_file/#examples). We have an example of [how this works with GitHub actions here](https://github.com/infracost/infracost-gh-action#multiple-aws-credentials).
- GCP users should set `GOOGLE_CREDENTIALS`, or read [this section](https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#full-reference) of the Terraform docs for other options.

### `INFRACOST_TERRAFORM_BINARY`

**Optional** Used to change the path to the `terraform` binary or version, see [this page](https://www.infracost.io/docs/integrations/environment_variables/#cicd-integrations) for the available options.

## Contributing

Issues and merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[Apache License 2.0](https://choosealicense.com/licenses/apache-2.0/)
